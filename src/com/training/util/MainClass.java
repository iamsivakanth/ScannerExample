package com.training.util;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Hey User! please enter a value (Numbers only): ");
		int a = scanner.nextInt();

		System.out.print("Hey User! please enter b value (Numbers only): ");
		int b = scanner.nextInt();

		int c = a > b ? b = 1000 : a < b ? a = 5000 : 6000;
		c += b;

		System.out.println("c = " + c);
	}
}